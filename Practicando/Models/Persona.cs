﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Practicando.Models
{
    public class Persona
    {
        //[Key]
        //public int Id { get; set; }

        //public string Nombre { get; set; }

        public int Edad { get; set; }
        //public DateTime FechaNacimiento { get; set; }

        public string Cedula { get; set; }

        public string Nombre { get; set; }

        public int CodigoDireccion { get; set; }
        public int Resumen { get; set; }
        public decimal Sueldo { get; set; }

        public Direccion Direccion { get; set; }
        public List<Curso> Cursos { get; set; }
    }
}