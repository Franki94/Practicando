﻿using Practicando.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Practicando.Models
{
    public class AplicationDbContext : DbContext
    {
        public DbSet<Persona> Personas { get; set; }
        public DbSet<Direccion> Direcciones { get; set; }

        public AplicationDbContext() : base("ConexionBecker")
        {

        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    //para cambiar las propiedades del tipo datetime en datetime2
        //    modelBuilder.Properties<DateTime>().Configure(x => x.HasColumnType("datetime2"));

        //    //para indicar que las propiedades que empiezan con Codigo son PK
        //    modelBuilder.Properties<int>().Where(p => p.Name.StartsWith("Codigo"))
        //        .Configure(p => p.IsKey());
        //    base.OnModelCreating(modelBuilder);
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //Toda direccion se ralaciona con una persona, no siempre una persona tiene una direccion
            modelBuilder.Entity<Direccion>().HasRequired(x => x.Persona).WithOptional(t => t.Direccion);

            //Toda direccion se relaciona con una persona, toda persona tiene una direccion
            modelBuilder.Entity<Direccion>().HasRequired(x => x.Persona).WithRequiredPrincipal(x => x.Direccion);

            //Toda tarjeta se relaciona con una persona, no siempre una persona tiene tarjeta
            modelBuilder.Entity<TarjetaDeCredito>().HasRequired(x => x.Persona);

            //Todocurso se relaciona con varias personas, toda persona se relaciona con varios cursos
            modelBuilder.Entity<Curso>().HasMany(x => x.Personas).WithMany(x => x.Cursos);

            //Personalizando la relacion de muchos a muchos
            modelBuilder.Entity<Curso>().HasMany(t => t.Personas).WithMany(x => x.Cursos)
                .Map(m =>
                {
                    m.ToTable("Persona_Curso");
                    m.MapLeftKey("IdCurso");
                    m.MapRightKey("IdPersona");
                });

            modelBuilder.Entity<Persona_Curso>().HasKey(x => new { x.PersonaId, x.CursoId });
            base.OnModelCreating(modelBuilder);
        }
        protected override bool ShouldValidateEntity(DbEntityEntry entityEntry)
        {
            //en este ejemplo pedimos validar a la hora de borrar
            if (entityEntry.State == EntityState.Deleted)
            {
                return true;
            }

            return base.ShouldValidateEntity(entityEntry);
        }

        protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry,
            IDictionary<object, object> items)
        {
            //si intentamos borrar un registro Persona
            if (entityEntry.Entity is Persona &&  entityEntry.State == EntityState.Deleted)
            {
                var entidad = entityEntry.Entity as Persona;
                if (entidad.Edad < 18)
                {
                    return new DbEntityValidationResult(entityEntry, new DbValidationError[]
                    {
                       new  DbValidationError("Edad","No se puede borrar a un menor de Edad")
                    });
                }
            }
            return base.ValidateEntity(entityEntry, items); 
        }

        public override int SaveChanges()
        {
            var entidades = ChangeTracker.Entries();
            if (entidades != null)
            {
                foreach (var entidad in entidades.Where(c => c.State != EntityState.Unchanged))
                {
                    Auditar(entidad);
                }
            }
            return base.SaveChanges();
        }

        private void Auditar(DbEntityEntry entidad)
        {
        }
    }
}

public class TarjetaDeCredito
{
    public Persona Persona { get; set; }
}
public class  Curso
{
    public  List<Persona> Personas { get; set; }
}

public class Persona_Curso
{
    public string PersonaId { get; set; }
    public int CursoId { get; set; }
}