﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Practicando.Models;

namespace Practicando.Controllers
{
    public class PersonasController : Controller
    {
        private AplicationDbContext db = new AplicationDbContext();

        // GET: Personas
        public ActionResult Index()
        {
            //Seleccionando todas las columnas
            var listadoPersonasTodasColumnas = db.Personas.ToList();

            //Seleccionando una columna
            var listadoDeNombres = db.Personas.Select(x => x.Nombre).ToList();

            //Selecciona varias columnas y proyectandolas a un tipo anonimo
            var listadoPersonasVariasColumnasAnonimo = db.Personas.Select(
                x => new { Nombre = x.Nombre, Edad = x.Edad}).ToList();

            //Selecciona varias columnas y proyectandolas hacia una persona
            var listadoPersonasVariasColumnas = db.Personas.Select(
                x => new { Nombre = x.Nombre, Edad = x.Edad }).ToList().
                Select(x => new Persona() {Nombre = x.Nombre, Edad = x.Edad }).ToList();

            return View(db.Personas.ToList());
        }

        // GET: Personas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,FechaNacimiento")] Persona persona)
        {
            //if (ModelState.IsValid)
            //{
            //    db.Personas.Add(persona);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            if (ModelState.IsValid)
            {
                var listaPersonas = new List<Persona>{persona};
                listaPersonas.Add(new Persona { Edad= 45, Nombre="Alex", FechaNacimiento = DateTime.Now});
                listaPersonas.Add(new Persona { Edad = 25, Nombre = "Alej", FechaNacimiento = DateTime.Now });

                db.Personas.AddRange(listaPersonas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(persona);
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,FechaNacimiento,Edad")] Persona persona)
        {
            if (ModelState.IsValid)
            {
                //Metodo 1: Traer el objeto y lo actualiza
                var personaAEditar = db.Personas.FirstOrDefault(x => x.Id == 2);
                personaAEditar.Nombre = "Editado metodo 1";
                personaAEditar.Edad = personaAEditar.Edad + 1;
                db.SaveChanges();

                //Metodo 2: Actualizacion Parcial
                var personaEditar2 = new Persona();
                personaEditar2.Id = 3;
                personaEditar2.Nombre = "Editado metodo 2";
                personaEditar2.Edad = 55;
                db.Personas.Attach(personaEditar2);
                db.Entry(personaEditar2).Property(x => x.Nombre).IsModified = true;
                db.SaveChanges();

                //Metodo 3: Objeto externo Actualizado
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(persona);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Persona persona = db.Personas.Find(id);
            db.Personas.Remove(persona);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
